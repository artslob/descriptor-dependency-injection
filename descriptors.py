from functools import partial


class SceneNotProvided(Exception):
    """ PostInit object was not inited with scene. """


class Scene:
    def __init__(self, magic):
        self._magic = magic

    @property
    def magic(self):
        return self._magic


class PostInitGlobal:
    """
    set `scene` attribute of this class and it will be shared with all others instances
    for example: PostInitGlobal.scene = Scene(...)
    """
    scene = None

    def __init__(self, f):
        self.f = f

    def __get__(self, instance, klass=None):
        if instance is None:
            # attribute is accessed from class
            return self

        if self.scene is None:
            raise SceneNotProvided

        return partial(self.f, instance, scene=self.scene)

    def __set__(self, instance, value):
        raise AttributeError


class PostInitLocal:
    """
    this descriptor is looking for object in __dict__ of instance with some magic name
    for example:
    model = SomeModel()
    model._scene_ = Scene(...)

    each instance have reference only to own scene
    """

    def __init__(self, f, magic_attr_name='_scene_'):
        self.f = f
        self.magic_attr_name = magic_attr_name

    def __get__(self, instance, klass=None):
        if instance is None:
            # attribute is accessed from class
            return self

        scene = instance.__dict__.get(self.magic_attr_name)
        if scene is None:
            raise SceneNotProvided

        return partial(self.f, instance, scene=scene)

    def __set__(self, instance, value):
        raise AttributeError


if __name__ == '__main__':
    class OrmModel:
        def __init__(self):
            self.a = 666

        @PostInitGlobal
        def post_inited_method(self, b, z, *, scene=None):
            print(self.a, scene.magic, b, z)


    model = OrmModel()
    PostInitGlobal.scene = Scene('magic string')
    print(model.post_inited_method(5, 6))
    print(OrmModel().post_inited_method(5, 6))
    # OrmModel.post_inited_method = 7
    # model.post_inited_method = 1
