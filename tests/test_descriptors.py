import pytest

from descriptors import PostInitGlobal, PostInitLocal, Scene, SceneNotProvided


def test_post_init_global(monkeypatch):
    class OrmModel:
        def __init__(self, a):
            self.a = a

        @PostInitGlobal
        def post_inited_method(self, b, z, *, scene=None):
            return self.a, scene.magic, b, z

    assert isinstance(OrmModel.post_inited_method, PostInitGlobal)

    model = OrmModel(666)
    with pytest.raises(SceneNotProvided):
        model.post_inited_method('was not', 'called')

    with pytest.raises(SceneNotProvided):
        OrmModel(2).post_inited_method('was not', 'called')

    # equals to:
    #   PostInitGlobal.scene = Scene('magic string')
    monkeypatch.setattr(PostInitGlobal, 'scene', Scene('magic string'))

    assert model.post_inited_method(5, 6) == (666, 'magic string', 5, 6)

    with pytest.raises(TypeError, match='missing 1 required positional argument'):
        model.post_inited_method(5)

    # scene is shared with others instances
    assert OrmModel(42).post_inited_method('bb', 'zz') == (42, 'magic string', 'bb', 'zz')


def test_post_init_local_to_instance():
    class OrmModel:
        def __init__(self, a):
            self.a = a

        @PostInitLocal
        def post_inited_method(self, b, z, *, scene=None):
            return self.a, scene.magic, b, z

    model = OrmModel(777)

    with pytest.raises(SceneNotProvided):
        model.post_inited_method('was not', 'called')

    model._scene_ = Scene('first scene')

    assert model.post_inited_method(3, 0) == (777, 'first scene', 3, 0)

    with pytest.raises(SceneNotProvided):
        # but others instances do not share scene
        OrmModel(128).post_inited_method('do not share', 'scene')

    second_model = OrmModel('a arg')
    second_model._scene_ = Scene('second scene')
    assert second_model.post_inited_method('b', 'z') == ('a arg', 'second scene', 'b', 'z')

    # first model still have reference to another scene
    assert model.post_inited_method('1', '2') == (777, 'first scene', '1', '2')
